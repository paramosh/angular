﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using AspNetCoreAng.Models;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AspNetCoreAng.Api
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        private AppDBContext context;
        public ValuesController(AppDBContext context)
        {
            this.context = context;
        }
        // GET: api/values/{1}
        [HttpGet("{id:int}")]
        public IActionResult GetById(int id)
        {
            var model = context.Appvals.Find(id);
            
            return Json(model);
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get()
        {
            var models = context.Appvals;



            var list = models.ToList();
            if (!list.Any())
            {
                return Json(NoContent());
            }


            return Json(list);
        }
        // POST: api/values
        [HttpPost]
        public IActionResult Post([FromBody]AppModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(BadRequest());
            }
            var r = context.Appvals.Select(x => x.AppId == model.AppId).FirstOrDefault();
            if (r)
            {
                context.Appvals.Update(model);
                context.SaveChanges();
            }
            else
            {
                context.Appvals.Add(model);
                context.SaveChanges();
            }
            
            

            return Json("ok");
        }
        // POST: api/values/2
        [HttpPut]
        public IActionResult Put([FromBody]AppModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(BadRequest());
            }
            bool recordExists = context.Appvals.Where(a => a.AppId == model.AppId).Any();
            if (!recordExists)
            {
                return Json(NoContent());
            }
            context.Appvals.Update(model);
            context.SaveChanges();


            return Json("ok");
        }
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await context.Appvals.FindAsync(id);

            if (model == null)
            {
                return Json(NotFound("Record not found; not deleted"));
            }
            context.Appvals.Remove(model);
            await context.SaveChangesAsync();
            return Json(Ok("deleted"));

        }
    }
}
