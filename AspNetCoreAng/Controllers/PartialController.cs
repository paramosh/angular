﻿using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreAng.Controllers
{
    public class PartialController : Controller
    {


        
        public IActionResult IndexComponent() => PartialView();
        public IActionResult AppDetailsComponent() => PartialView();
        public IActionResult SidebarComponent() => PartialView();
        public IActionResult NavbarComponent() => PartialView();
        public IActionResult ApplicationsListComponent() => PartialView();

    }
}
