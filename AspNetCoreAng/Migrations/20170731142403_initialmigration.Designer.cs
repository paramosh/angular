﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using AspNetCoreAng.Models;

namespace AspNetCoreAng.Migrations
{
    [DbContext(typeof(AppDBContext))]
    [Migration("20170731142403_initialmigration")]
    partial class initialmigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("AspNetCoreAng.Models.Appval", b =>
                {
                    b.Property<int>("AppId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AppName");

                    b.Property<string>("Description");

                    b.Property<string>("Password");

                    b.Property<string>("Url");

                    b.Property<string>("UserName");

                    b.HasKey("AppId");

                    b.ToTable("Appvals");
                });
        }
    }
}
