﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using AspNetCoreAng.Models;

namespace AspNetCoreAng.Migrations
{
    [DbContext(typeof(AppDBContext))]
    partial class AppDBContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("AspNetCoreAng.Models.ActionModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<int>("Protocol");

                    b.Property<string>("SampleData");

                    b.Property<string>("Title");

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.ToTable("Actions");
                });

            modelBuilder.Entity("AspNetCoreAng.Models.AppModel", b =>
                {
                    b.Property<int>("AppId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AppName");

                    b.Property<string>("Description");

                    b.Property<string>("Password");

                    b.Property<string>("Url");

                    b.Property<string>("UserName");

                    b.HasKey("AppId");

                    b.ToTable("Appvals");
                });

            modelBuilder.Entity("AspNetCoreAng.Models.TriggerModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Key");

                    b.Property<int>("Protocol");

                    b.Property<string>("Title");

                    b.Property<string>("TriggerBody");

                    b.Property<string>("Url");

                    b.HasKey("Id");

                    b.ToTable("Trigger");
                });
        }
    }
}
