﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreAng.Models
{
    public class AppDBContext:DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }
        public DbSet<AppModel> Appvals { get; set; }
        public DbSet<ActionModel> Actions { get; set; }
        public DbSet<TriggerModel> Trigger { get; set; }
    }
}
