﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreAng.Models
{
    public class TriggerModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Protocol { get; set; }
        public string Key{ get; set; }
        public string Url { get; set; }
        public string TriggerBody { get; set; }

    }
}
