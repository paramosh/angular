﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ActionsComponent } from './actions.component';
import { ActionDetailsComponent } from './action-datails.component';
import { ActionsListComponent } from './actions-list.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: ActionsComponent,
                children: [
                    {
                        path: '',
                        component: ActionsListComponent
                    },
                    {
                        path: ':id',
                        component: ActionDetailsComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class ActionsRoutingModule {
}