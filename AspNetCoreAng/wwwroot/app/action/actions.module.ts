﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionsComponent } from './actions.component';
import { ActionDetailsComponent } from './action-datails.component';
import { ActionsListComponent } from './actions-list.component';
import { ActionsRoutingModule } from './actions-routing.module';

@NgModule({
    imports: [
        CommonModule,
        ActionsRoutingModule
    ],
    declarations: [
        ActionsComponent,
        ActionDetailsComponent,
        ActionsListComponent
    ]
})
export class  ActionsModule {
}