﻿import { RouterModule } from '@angular/router';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { IndexComponent } from './index.component';


@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: '', redirectTo: '/home', pathMatch: 'full' },
            { path: 'home', component: IndexComponent },
            { path: 'applications', loadChildren: 'app/application/applications.module#ApplicationsModule' }
        ])
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule {
}