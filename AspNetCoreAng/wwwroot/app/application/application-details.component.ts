﻿import { Component,OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { AppModel } from '../models/AppModel';
import { DataService } from '../services/data.service';
import { NgModel } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    templateUrl: '/partial/AppDetailsComponent',
    providers: [DataService]

})

export class ApplicationDetailsComponent implements OnInit {
        

    public model: AppModel = new AppModel();

    constructor(private dataservice: DataService, private router: Router, private _route: ActivatedRoute) {

    }



    AddApp() {

        this.dataservice.addData(this.model).subscribe();
        this.router.navigate([]);
    }
    ChangeApp() {

    }
    ngOnInit(): void {
        let id = +this._route.snapshot.paramMap.get('id');
        this.dataservice.getById(id).subscribe((values: AppModel) => {
            this.model = values as AppModel;
        });
    }


}


