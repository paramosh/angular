﻿import { Component, OnInit} from '@angular/core';
import { AppModel } from '../models/AppModel';
import { DataService } from '../services/data.service';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { NgForm } from "@angular/forms";



@Component({
    templateUrl: '/partial/ApplicationsListComponent',
    providers: [DataService]
    
})

export class ApplicationsListComponent implements OnInit {

    public appmodels: AppModel[];
   
    constructor(private router: Router, private dataservice: DataService) {
    }

    ngOnInit() {

        this.dataservice.getData().subscribe((values: AppModel[]) => {
            this.appmodels = values as AppModel[];
        });
        console.log("init");

    }

    onSelect(index:number) {
        this.router.navigate(['/applications', this.appmodels[index].AppId]);
    }
}

