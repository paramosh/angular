﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ApplicationComponent } from './application.component';
import { ApplicationDetailsComponent } from './application-details.component';
import { ApplicationsListComponent } from './applications-list.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: ApplicationComponent,
                children: [
                    {
                        path: '',
                        component: ApplicationsListComponent
                       
                    },
                    {
                        path: ':id',
                        component: ApplicationDetailsComponent,
                    }
                    //{
                    //    path: 'actionss', loadChildren: 'app/action/actions.module#ActionsModule'
                    //},
                    //{
                    //    path: 'triggers', loadChildren: 'app/trigger/triggers.module#TriggersModule'
                    //}

                ]
            },

        ])

    ],
    exports: [
        RouterModule
    ]
})
export class ApplicationsRoutingModule {
}