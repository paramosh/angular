﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApplicationComponent } from './application.component';
import { ApplicationDetailsComponent } from './application-details.component';
import { ApplicationsListComponent } from './applications-list.component';
import { ApplicationsRoutingModule } from './applications-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        ApplicationsRoutingModule,
        FormsModule
    ],
    declarations: [
        ApplicationComponent,
        ApplicationDetailsComponent,
        ApplicationsListComponent
    ]
})
export class ApplicationsModule {
}