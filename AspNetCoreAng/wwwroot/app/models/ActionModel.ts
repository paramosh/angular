﻿export class ActionModel {

    Id: number;
    Title: string;
    Description: string;
    Protocol: number;
    Url: string;
    SampleData: string;
}