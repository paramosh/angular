﻿import { Component } from '@angular/core';


export class AppModel {
    AppId: number;
    AppName: string;
    Description: string;
    Url: string;
    UserName: string;
    Password: string;
    
}