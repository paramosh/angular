﻿export class TriggerModel {
    Id: number;
    Title: string;
    Description: string;
    Protocol: number;
    Url: string;
    Key: string;
    TriggerBody:  string;
}