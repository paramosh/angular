﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { AppModel } from '../models/AppModel';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';



@Injectable()
export class DataService {

    private url: string = 'api/values';

    constructor(private http: Http) { }

    getData = () => {
        return this.http.get(this.url)
            .map((resp: Response) => <AppModel[]>resp.json())
            .catch(this.handleError);
    }
    getById = (id:number) => {
        return this.http.get(this.url+'/'+id)
            .map((resp: Response) => <AppModel>resp.json())
            .catch(this.handleError);
    }

    addData(appModel: AppModel) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json'); // also tried
        return this.http
            .post(this.url, JSON.stringify(appModel), { headers: headers })
            .map((resp: Response) => resp.json())
            .catch(this.handleError);
    }

    editData(appModel: AppModel) {
        return this.http
            .put(this.url, JSON.stringify(appModel) )
            .map((resp: Response) => resp.json())
            .catch(this.handleError);
    }

    deleteRecord(itemToDelete: AppModel) {
        return this.http.delete(this.url + '/' + itemToDelete.AppId )
            .map((res: Response) => res.json())
            .catch(this.handleError);
    }

    // from https://angular.io/docs/ts/latest/guide/server-communication.html
    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}