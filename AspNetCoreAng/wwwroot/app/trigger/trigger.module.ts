﻿import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TriggerComponent } from './trigger.component';
import { TriggerDetailsComponent } from './trigger-details.component';
import { TriggerListComponent } from './triggers-list.component';
import { TriggersRoutingModule } from './triggers-routing.module';


@NgModule({
    imports: [
        CommonModule,
        TriggersRoutingModule
    ],
    declarations: [
        TriggerComponent,
        TriggerDetailsComponent,
        TriggerListComponent
    ]
})
export class TasksModule {
}