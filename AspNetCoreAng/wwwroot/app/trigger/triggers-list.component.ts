﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    templateUrl: ''
})

export class TriggerListComponent {

    constructor(private router: Router) {
    }

    private tasks = [
        { id: '1', title: 'Code Cleanup' },
        { id: '2', title: 'Review Code' },
        { id: '3', title: 'Build to Prod' }
    ];
    private errorMessage: any = '';

    onSelect() {
        this.router.navigate(['/tasks']);
    }
}