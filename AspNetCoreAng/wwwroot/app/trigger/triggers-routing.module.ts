﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TriggerComponent } from './trigger.component';
import { TriggerDetailsComponent } from './trigger-details.component';
import { TriggerListComponent } from './triggers-list.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: TriggerComponent,
                children: [
                    {
                        path: '',
                        component: TriggerListComponent
                    },
                    {
                        path: ':id',
                        component: TriggerDetailsComponent
                    }
                ]
            }
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class TriggersRoutingModule {
}